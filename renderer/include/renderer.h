//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef RENDERER__H
#define RENDERER__H

#include <glass_types.h>

#include <QObject>
#include <display_plane.h>
#include <gpu.h>
#include <vm.h>

#include <overlay.h>
#include <window_manager.h>

namespace banner
{
namespace position
{
constexpr const auto none = 1;
constexpr const auto top = 2;
constexpr const auto bottom = 3;
constexpr const auto both = 4;
};

constexpr const auto max_height = 50;
};

class renderer_t : public QObject
{
  Q_OBJECT
public:
  renderer_t(window_manager_t &wm) :m_wm(wm) {}
    virtual ~renderer_t() = default;

public slots:
    virtual void add_guest(std::shared_ptr<vm_render_t> vm) = 0;
    virtual void remove_guest(std::shared_ptr<vm_t> vm) = 0;

    virtual void dpms_on() = 0;
    virtual void dpms_off() = 0;

    virtual void refresh() = 0;
    virtual void reset() = 0;
    virtual void save_screenshot() = 0;

    virtual void render() = 0;

    virtual desktop_plane_t *desktop(uuid_t uuid) = 0;

protected:
    window_manager_t &m_wm;
};

#endif // RENDERER__H
