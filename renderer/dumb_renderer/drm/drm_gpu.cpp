//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "drm_gpu.h"
#include <algorithm>

bool
sort_displays(json &l, json &r)
{
    return l["edid_hash"] < r["edid_hash"];
}

uint qHash(QSize key)
{
    return qHash(qHash(key.width()) ^ qHash(key.height()) ^ 0x95782);
}

// Populates an empty json object with the current configurations available
// for the GPU in device_path, and its associated monitors and connectors.
drm_gpu_t::drm_gpu_t(json &config, std::string &device_path) : m_mode_resource(nullptr, drmModeFreeResources)
{
  TRACE;
    int rc = 0;

    m_drm_fd = open(device_path.c_str(), O_RDWR);

    Expects(m_drm_fd > 0);

    // Take master control of the DRM device
    rc = drmSetMaster(m_drm_fd);
    if (rc) {
        throw std::runtime_error("Failed to set DRM master");
    }

    // Obtain the mode resources available for the card
    m_mode_resource = std::shared_ptr<drmModeRes>(drmModeGetResources(m_drm_fd), drmModeFreeResources);

    // Setup resource spans
    m_crtc_span = gsl::span<uint32_t>(m_mode_resource->crtcs, m_mode_resource->count_crtcs);
    m_connector_span = gsl::span<uint32_t>(m_mode_resource->connectors, m_mode_resource->count_connectors);
    m_encoder_span = gsl::span<uint32_t>(m_mode_resource->encoders, m_mode_resource->count_encoders);

    // Obtain the plane resources for the card
    m_plane_resource =
        std::shared_ptr<drmModePlaneRes>(drmModeGetPlaneResources(m_drm_fd), drmModeFreePlaneResources);

    // Setup plane span
    m_plane_span = gsl::span<uint32_t>(m_plane_resource->planes, m_plane_resource->count_planes);

    create_planes();
    create_connectors();
    create_crtcs();
    create_encoders();

    std::shared_ptr<drmVersion> version(drmGetVersion(m_drm_fd), drmFreeVersion);

    config["device_path"] = device_path;
    config["desktop_type"] = "shared";
    config["name"] = version->name;
    config["renderable"] = true;
    std::string config_hash;
    int x_count = 0;
    for (auto connector : m_connectors.values()) {
        json e_display;
        e_display["name"] = connector->name().toStdString();
        e_display["x"] = x_count;
        e_display["y"] = 0;
        e_display["uuid"] = "none";
        e_display["preferred_mode_index"] = 0;
        e_display["edid_hash"] = QString::number(connector->get_edid_hash(), 16).toStdString();

        for (auto mode : connector->modes()) {
            if(mode->resolution_width() >= 800
               && mode->resolution_height() >= 600) {
                json e_mode;

                e_mode["name"] = mode->name(true).toStdString();
                e_mode["width"] = mode->resolution_width();
                e_mode["height"] = mode->resolution_height();
                e_display["modes"].push_back(e_mode);
            }
        }

        if (!e_display["modes"].is_null()) {
            int x_stride = e_display["modes"][0]["width"];
            x_count += x_stride;
            config["displays"].push_back(e_display);
        }
    }

    // DRM enumeration isn't ordered, so to create consistant hashes, sort
    // the displays by edid_hash value.
    std::sort(config["displays"].begin(), config["displays"].end(), sort_displays);

    for (auto i_display : config["displays"]) {
        config_hash += i_display["name"];
        config_hash += i_display["edid_hash"];
    }

    config["config_hash"] = QString::number(qHash(QString::fromStdString(config_hash)), 16).toStdString();
}

drm_gpu_t::drm_gpu_t(json &gpu_config, int banner_height) :
  gpu_t(gpu_config),
  m_banner_height(banner_height),
  m_mode_resource(nullptr, drmModeFreeResources)
{
  TRACE;
    int rc = 0;
    const std::string device_path = gpu_config["device_path"];
    const std::string desktop_type = gpu_config["desktop_type"];

    m_drm_fd = open(device_path.c_str(), O_RDWR);

    Expects(m_drm_fd > 0);

    // Take master control of the DRM device
    rc = drmSetMaster(m_drm_fd);
    if (rc) {
        throw std::runtime_error("Failed to set DRM master");
    }

    // Obtain the mode resources available for the card
    m_mode_resource = std::shared_ptr<drmModeRes>(drmModeGetResources(m_drm_fd), drmModeFreeResources);

    // Setup resource spans
    m_crtc_span = gsl::span<uint32_t>(m_mode_resource->crtcs, m_mode_resource->count_crtcs);
    m_connector_span = gsl::span<uint32_t>(m_mode_resource->connectors, m_mode_resource->count_connectors);
    m_encoder_span = gsl::span<uint32_t>(m_mode_resource->encoders, m_mode_resource->count_encoders);

    // Obtain the plane resources for the card
    m_plane_resource =
        std::shared_ptr<drmModePlaneRes>(drmModeGetPlaneResources(m_drm_fd), drmModeFreePlaneResources);

    // Setup plane span
    m_plane_span = gsl::span<uint32_t>(m_plane_resource->planes, m_plane_resource->count_planes);

    create_planes();
    create_connectors();
    create_crtcs();
    create_encoders();

    if (desktop_type == "cloned") {
        setup_cloned_monitors(gpu_config["displays"]);
    } else if (desktop_type == "shared") {
        setup_shared_monitors(gpu_config["displays"]);
    }

    // There can only be one desktop type. The pinned monitors are
    // not considered part of that desktop, they are a set of desktops
    // handled individually.
    setup_pinned_monitors(gpu_config["displays"]);

    if(pinned_desktops().size()) {
        vg_debug() << "Pinned desktop exists";
        vg_debug() << pinned_desktops().size();
        for(auto &pinned : pinned_desktops()) {
            if(!pinned) {
                continue;
            }

            vg_debug() << pinned->origin() << pinned->rect();
        }
    }

    if(shared_desktop()) {
        vg_debug() << "Shared desktop exists";
        vg_debug() << shared_desktop()->origin() << shared_desktop()->rect();
    }

    if(cloned_desktop()) {
        vg_debug() << "Cloned desktop exists.";
        vg_debug() << cloned_desktop()->origin() << cloned_desktop()->rect();
    }
}

drm_gpu_t::~drm_gpu_t()
{
  TRACE;
  drmDropMaster(m_drm_fd);
}

std::shared_ptr<desktop_plane_t>
drm_gpu_t::cloned_desktop()
{
    if (m_cloned_display_planes.size() == 0) {
        return nullptr;
    }

    if(!m_cloned_desktop) {
        vg_debug() << "Creating the cloned desktop";
        m_cloned_desktop = m_shared_desktop = std::make_shared<desktop_plane_t>();
    } else {
        return m_cloned_desktop;
    }

    if (!m_cloned_desktop) {
        return nullptr;
    }

    m_cloned_desktop->lock()->lock();
    for (auto display_plane : m_cloned_display_planes) {
      m_cloned_desktop->add_display(display_plane);
      break;
    }

    m_cloned_desktop->reorigin_displays();
    m_cloned_desktop->translate_planes();
    m_cloned_desktop->lock()->unlock();

    return m_cloned_desktop;
}

std::shared_ptr<desktop_plane_t>
drm_gpu_t::shared_desktop()
{
    if (m_shared_display_planes.size() == 0) {
        return nullptr;
    }

    if(!m_shared_desktop) {
        vg_debug() << "Creating the shared desktop";
        m_shared_desktop = m_cloned_desktop = std::make_shared<desktop_plane_t>();
    } else {
        return m_shared_desktop;
    }

    if (!m_shared_desktop) {
        return nullptr;
    }

    m_shared_desktop->lock()->lock();
    for (auto display_plane : m_shared_display_planes) {
      m_shared_desktop->add_display(display_plane);
    }

    m_shared_desktop->reorigin_displays();
    m_shared_desktop->translate_planes();
    m_shared_desktop->lock()->unlock();
    return m_shared_desktop;
}

qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>>
drm_gpu_t::pinned_desktops()
{
    if (m_pinned_display_planes.keys().size() == 0) {
        return m_pinned_desktops;
    }

    for (auto uuid : m_pinned_display_planes.keys()) {
        if (m_pinned_display_planes[uuid].size() == 0) {
            continue;
        }

        if (!m_pinned_desktops[uuid]) {
            vg_debug() << "Creating a pinned desktop: " << uuid;
            auto desktop_plane = std::make_shared<desktop_plane_t>(uuid);

            if (!desktop_plane) {
                return m_pinned_desktops;
            }
            desktop_plane->lock()->lock();
            for (auto display_plane : m_pinned_display_planes[uuid]) {
                desktop_plane->add_display(display_plane);
            }

            desktop_plane->reorigin_displays();
            desktop_plane->translate_planes();
            desktop_plane->lock()->unlock();
            m_pinned_desktops[uuid] = desktop_plane;
        }
    }

    return m_pinned_desktops;
}

void
drm_gpu_t::disable_display(display_plane_t &display)
{
    (void) display;
}

void
drm_gpu_t::enable_display(display_plane_t &display)
{
    (void) display;
}

void
drm_gpu_t::show_cursor(display_plane_t &display, std::shared_ptr<cursor_t> cursor)
{
    display.show_cursor(cursor);
}

void
drm_gpu_t::move_cursor(display_plane_t &display, point_t point)
{
    display.move_cursor(point);
}

void
drm_gpu_t::hide_cursor(display_plane_t &display)
{
    display.hide_cursor();
}

void
drm_gpu_t::associate_crtcs_to_connectors(const qlist_t<std::shared_ptr<drm_connector_t>> &connectors,
                                         qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> &crtcid_lookup,
                                         qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> &connector_lookup)
{
    // Find a suitable crtc
    for (auto connector : connectors) {
        gsl::span<uint32_t> encoder_span(connector->connector()->encoders,
                                         connector->connector()->count_encoders);

        for (auto encoder_id : encoder_span) {
            if (!m_encoders_available.contains(encoder_id)) {
                continue;
            }

            if(crtcid_lookup.keys().contains(connector)) {
                continue;
            }

            std::unique_ptr<drmModeEncoder, void (*)(drmModeEncoder *)> encoder(
                drmModeGetEncoder(m_drm_fd, encoder_id), drmModeFreeEncoder);

            for (auto crtc_id : m_crtc_span) {
                if (!(encoder->possible_crtcs & m_crtcs[crtc_id]->bitmask())) {
                    continue;
                }

                if (!m_crtcs_available.contains(crtc_id)) {
                    continue;
                }

                m_encoders_available.removeAll(encoder_id);
                m_crtcs_available.removeAll(crtc_id);

                crtcid_lookup[connector] = crtc_id;
                connector->set_crtc_id(crtc_id);
                connector_lookup[crtc_id] = connector;
                break;
            }

            if (crtcid_lookup[connector] == 0) {
                qDebug() << "Failed to find a suitable CRTC";
                return;
            }

            for (auto plane_id : m_plane_span) {
                std::unique_ptr<drmModePlane, void (*)(drmModePlane *)> plane(
                    drmModeGetPlane(m_drm_fd, plane_id), drmModeFreePlane);

                for (auto crtc_id : m_crtc_span) {
                    if (!(plane->possible_crtcs & m_crtcs[crtc_id]->bitmask())) {
                        continue;
                    }

                    if (crtc_id != crtcid_lookup[connector]) {
                        continue;
                    }

                    m_planes_available.removeAll(plane_id);

                    break;
                }
            }
        }
    }

    return;
}

void
drm_gpu_t::crtc_connector_join(std::shared_ptr<drm_connector_t> connector,
                               uint32_t crtc_id,
                               point_t origin,
                               QSize resolution,
                               std::shared_ptr<dumb_fb_t> pfb,
                               std::shared_ptr<dumb_fb_t> pcursor)
{
    std::shared_ptr<dumb_fb_t> fb;
    std::shared_ptr<dumb_fb_t> cursor;
    if(!pfb) {
        fb = std::make_shared<dumb_fb_t>(m_drm_fd, resolution);
    } else {
        fb = pfb;
    }

    if(!pcursor) {
        cursor = std::make_shared<dumb_fb_t>(m_drm_fd, QSize(64, 64));
    } else {
        cursor = pcursor;
    }

    m_crtcs[crtc_id]->add_fb(fb);
    m_crtcs[crtc_id]->add_cursor(cursor);
    m_crtcs[crtc_id]->add_connector(connector);
    m_crtcs[crtc_id]->set_resolution(connector, resolution);
    m_crtcs[crtc_id]->set_position(origin);
    m_crtcs[crtc_id]->set_unique_id(connector->get_edid_hash());
}

QSize
drm_gpu_t::get_cloned_resolution(json &displays)
{
    QSize resolution(-1, -1);
    qset_t<QSize> available_modes;

    bool panel_fitter_available = false;
    for (auto connector : m_connectors) {
        if(connector->panel_fitter()) {
            panel_fitter_available = true;
        }
    }

    // Find the compatible connectors in the configuration
    for (auto monitor : displays) {
        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string connector_name = monitor["name"];

        // Find compatible connectors

        for (auto connector : m_connectors) {
            qset_t<QSize> connector_modes;
            if (m_cloned_connectors.contains(connector)) {
                continue;
            }

            if (connector->name() == QString(connector_name.c_str())) {
                m_cloned_connectors.append(connector);
                if((!connector->panel_fitter() && panel_fitter_available)
                   || connector->modes().size() > 1) {
                    for(auto mode : monitor["modes"]) {
                        connector_modes.insert(QSize(mode["width"],
                                                     mode["height"]));
                    }

                    if(available_modes.isEmpty()) {
                        available_modes += connector_modes;
                    } else {
                        available_modes &= connector_modes;
                    }

                    QSize preferred_mode = QSize(monitor["modes"][preferred_mode_index]["width"],
                                                 monitor["modes"][preferred_mode_index]["height"]);
                    if(available_modes.contains(preferred_mode)) {
                        resolution = preferred_mode;
                    } else {
                        resolution = QSize(-1,-1);
                    }
                } else {
                    vg_info() << "WARNING: Skipped connector" << connector->name();
                }
            }
        }
    }

    if(resolution == QSize(-1, -1)) {
        QSize max_compatible_mode(1024, 768);
        for(auto tmp : available_modes) {
            if((tmp.width() * tmp.height()) > (max_compatible_mode.width() * max_compatible_mode.height())) {
                max_compatible_mode = tmp;
            }
        }

        resolution = max_compatible_mode;
    }

    return resolution;
}

void
drm_gpu_t::setup_cloned_monitors(json &displays)
{
    QSize resolution(-1, -1);
    point_t origin(0,0);

    if(displays.size() == 1) {
        setup_shared_monitors(displays);
        return;
    }

    resolution = get_cloned_resolution(displays);

    std::shared_ptr<dumb_fb_t> fb = std::make_shared<dumb_fb_t>(m_drm_fd, resolution);
    std::shared_ptr<dumb_fb_t> cursor = std::make_shared<dumb_fb_t>(m_drm_fd, QSize(64, 64), QImage::Format_ARGB32);

    // Setup lookup and reverse lookup
    qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
    qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;

    associate_crtcs_to_connectors(m_cloned_connectors, crtcid_lookup, connector_lookup);

    std::shared_ptr<drm_connector_t> panel_fit_connector = nullptr;
    std::shared_ptr<drm_crtc_t> crtc = nullptr;

    if(m_cloned_connectors.size() == 1) {
        vg_debug() << "Only one connector, cloned doesn't make a whole lot of sense";
        auto connector = m_cloned_connectors.front();
        if (connector->is_connected() && connector->mode(resolution)) {
            crtc_connector_join(connector,
                                crtcid_lookup[connector],
                                origin,
                                resolution,
                                fb, cursor);
            m_crtcs[crtcid_lookup[connector]]->set_name("CLONED");
            m_cloned_display_planes.push_back(m_crtcs[crtcid_lookup[connector]]);
            return;
        }
    }

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected() && connector->panel_fitter()) {
            panel_fit_connector = connector;
            break;
        }
    }

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected()) {
            crtc = m_crtcs[crtcid_lookup[connector]];
            break;
        }
    }

    uint32_t cloned_edid = 0;

    for(auto connector : connector_lookup.values()) {
        if(!connector->mode(resolution)) {
            continue;
        }

        cloned_edid ^= connector->get_edid_hash();
    }

    if(panel_fit_connector) {
        if((resolution.width() == 3840 && resolution.height() == 2160) &&
           !panel_fit_connector->mode(resolution)) {
            // Panel fitting 4k to a non-4k laptop monitor produces poor results
            resolution = QSize(1920, 1080);
        }

        crtc->add_fb(fb);
        crtc->add_cursor(cursor);
        crtc->set_unique_id(cloned_edid);
        crtc->set_position(origin);

        std::shared_ptr<drm_connector_t> tmp_connector = nullptr;

        for(auto connector : connector_lookup.values()) {
            if(connector->is_connected() && connector != panel_fit_connector) {
                if(!connector->mode(resolution)) {
                    continue;
                }

                crtc->add_connector(connector);
                crtc->set_resolution(connector, connector->mode(resolution)->modeinfo());
                tmp_connector = connector;
            }
        }

        crtc->add_connector(panel_fit_connector);
        crtc->set_resolution(panel_fit_connector, tmp_connector->mode(resolution)->modeinfo());
        crtc->set_name("CLONED");
        m_cloned_display_planes.push_back(crtc);
    } else {
        crtc->add_fb(fb);
        crtc->add_cursor(cursor);
        crtc->set_unique_id(cloned_edid);
        crtc->set_position(origin);
        for(auto connector : connector_lookup.values()) {
            if(connector->is_connected() && connector != panel_fit_connector) {
                if(!connector->mode(resolution)) {
                    continue;
                }

                crtc->add_connector(connector);
                crtc->set_resolution(connector, connector->mode(resolution)->modeinfo());
            }
        }
        crtc->set_name("CLONED");
        m_cloned_display_planes.push_back(crtc);
    }

    // Software cloned mode... yuck
    if(!panel_fit_connector && !crtc && m_cloned_display_planes.size() == 0) {
        fb = nullptr;
        cursor = nullptr;
        for(auto connector : connector_lookup.values()) {
            if (connector->is_connected()
                && !connector->panel_fitter()
                && m_cloned_connectors.size() >= 1) {
                crtc_connector_join(connector,
                                    crtcid_lookup[connector],
                                    origin,
                                    resolution,
                                    nullptr, nullptr);
                crtc->set_name("CLONED");
                m_cloned_display_planes.push_back(m_crtcs[crtcid_lookup[connector]]);
            }
        }
    }
}

void
drm_gpu_t::setup_shared_monitors(json &displays)
{
    qmap_t<std::shared_ptr<drm_connector_t>, QSize> connector_resolution_lookup;
    qmap_t<std::shared_ptr<drm_connector_t>, point_t> connector_origin_lookup;
    for (auto monitor : displays) {
        std::string uuid = monitor["uuid"];
        if (uuid != "none") {
            continue;
        }

        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string name = monitor["name"];
        QSize resolution(monitor["modes"][preferred_mode_index]["width"],
                         monitor["modes"][preferred_mode_index]["height"]);
        point_t origin(monitor["x"], monitor["y"]);

        for (auto connector : m_connectors) {
            if (connector->name().toStdString() == name && connector->mode(resolution)) {
                connector_resolution_lookup[connector] = resolution;
                connector_origin_lookup[connector] = origin;
                m_shared_connectors.append(connector);
            }
        }
    }

    // Setup lookup and reverse lookup
    qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
    qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;

    associate_crtcs_to_connectors(m_shared_connectors, crtcid_lookup, connector_lookup);

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected() && connector->mode(connector_resolution_lookup[connector])) {
            crtc_connector_join(connector,
                                crtcid_lookup[connector],
                                connector_origin_lookup[connector],
                                connector_resolution_lookup[connector],
                                nullptr, nullptr);
            m_shared_display_planes.push_back(m_crtcs[crtcid_lookup[connector]]);
        }
    }
}

void
drm_gpu_t::setup_pinned_monitors(json &displays)
{
    qmap_t<std::shared_ptr<drm_connector_t>, QSize> connector_resolution_lookup;
    qmap_t<std::shared_ptr<drm_connector_t>, point_t> connector_origin_lookup;

    for (auto monitor : displays) {
        std::string uuid = monitor["uuid"];
        if (uuid == "none") {
            continue;
        }

        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string name = monitor["name"];
        QSize resolution(monitor["modes"][preferred_mode_index]["width"],
                         monitor["modes"][preferred_mode_index]["height"]);
        point_t origin(monitor["x"], monitor["y"]);

        for (auto connector : m_connectors) {
            if (connector->name().toStdString() == name && connector->mode(resolution)) {
                connector_resolution_lookup[connector] = resolution;
                connector_origin_lookup[connector] = origin;
                m_pinned_connectors[uuid_t(QString::fromStdString(uuid))].push_back(connector);
            }
        }
    }

    for (auto uuid : m_pinned_connectors.keys()) {
        qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
        qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;
        qlist_t<std::shared_ptr<display_plane_t>> display_planes;

        associate_crtcs_to_connectors(m_pinned_connectors[uuid], crtcid_lookup, connector_lookup);

        for (auto connector : connector_lookup.values()) {
            if (connector->is_connected() && connector->mode(connector_resolution_lookup[connector])) {
                crtc_connector_join(connector,
                                    crtcid_lookup[connector],
                                    connector_origin_lookup[connector],
                                    connector_resolution_lookup[connector],
                                    nullptr, nullptr);
                display_planes.push_back(m_crtcs[crtcid_lookup[connector]]);
            }
        }
        m_pinned_display_planes.insert(uuid, display_planes);
    }
}

void
drm_gpu_t::create_planes()
{
    for (auto plane_id : m_plane_span) {
        m_planes_available.append(plane_id);
    }
}

void
drm_gpu_t::create_connectors()
{
    for (auto connector_id : m_connector_span) {
        std::unique_ptr<drmModeConnector, void (*)(drmModeConnector *)> connector(
            drmModeGetConnector(m_drm_fd, connector_id), drmModeFreeConnector);
        m_connectors[connector_id] = std::make_shared<drm_connector_t>(m_drm_fd, std::move(connector));
        m_connectors_available.append(connector_id);
    }
}

void
drm_gpu_t::create_crtcs()
{
    // Best to distinguish by CRTC first, and narrow the scope of available
    // configurations from there (it reduces the set of configurations in the
    // quickest manner).
    for (auto crtc_id : m_crtc_span) {
        // Before we create the CRTC and by extension create the dumb buffer,
        // distinguish what the available connectors for this particular crtc_id.
        // We'll pass that information to the CRTC in a span that contains the
        // connector_ids to use.
        m_crtcs[crtc_id] = std::make_shared<drm_crtc_t>(m_drm_fd, crtc_id, m_mode_resource, m_banner_height);
        m_crtcs_available.append(crtc_id);
    }
}

void
drm_gpu_t::create_encoders()
{
    for (auto encoder_id : m_encoder_span) {
        m_encoders_available.append(encoder_id);
    }
}

void
drm_gpu_t::apply_to_crtcs(std::function<void(int, uint32_t)> fn)
{
    for (auto crtc_id : m_crtc_span) {
        fn(m_drm_fd, crtc_id);
    }
}

void
drm_gpu_t::apply_to_connectors(std::function<void(int, uint32_t)> fn)
{
    for (auto connector_id : m_connector_span) {
        fn(m_drm_fd, connector_id);
    }
}

void
drm_gpu_t::apply_to_encoders(std::function<void(int, uint32_t)> fn)
{
    for (auto encoder_id : m_encoder_span) {
        fn(m_drm_fd, encoder_id);
    }
}

void
drm_gpu_t::apply_to_modes(uint32_t conn_id, std::function<void(uint32_t)> fn)
{
    (void) conn_id;
    (void) fn;
}
