//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DRM_GPU__H
#define DRM_GPU__H

extern "C" {
#include <fcntl.h>
#include <libdrm/drm.h>
#include <libdrm/drm_mode.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
}

#include "drm_connector.h"
#include "drm_crtc.h"
#include "dumb_fb.h"
#include <gpu.h>

class drm_gpu_t : public gpu_t
{
public:
  // This is a constructor only for probing modes for a particular GPU
  drm_gpu_t(json &gpu_config, std::string &device_path);
  drm_gpu_t(json &gpu_config, int banner_height = 0);
  ~drm_gpu_t();

  std::shared_ptr<desktop_plane_t> shared_desktop();
  std::shared_ptr<desktop_plane_t> cloned_desktop();
  qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> pinned_desktops();

  void disable_display(display_plane_t &display);
  void enable_display(display_plane_t &display);

  void show_cursor(display_plane_t &display, std::shared_ptr<cursor_t> cursor);
  void move_cursor(display_plane_t &display, point_t point);
  void hide_cursor(display_plane_t &display);

private:
  void create_planes();
  void create_connectors();
  void create_crtcs();
  void create_encoders();

  void associate_crtcs_to_connectors(const qlist_t<std::shared_ptr<drm_connector_t>> &connectors,
				     qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> &crtcid_lookup,
				     qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> &connector_lookup);
  void crtc_connector_join(std::shared_ptr<drm_connector_t> connector,
                           uint32_t crtc_id,
                           point_t origin,
                           QSize resolution,
                           std::shared_ptr<dumb_fb_t> pfb,
                           std::shared_ptr<dumb_fb_t> pcursor);

  QSize get_cloned_resolution(json &displays);

  void setup_cloned_monitors(json &displays);
  void setup_shared_monitors(json &displays);
  void setup_pinned_monitors(json &displays);

  void apply_to_crtcs(std::function<void(int, uint32_t)> fn);
  void apply_to_connectors(std::function<void(int, uint32_t)> fn);
  void apply_to_encoders(std::function<void(int, uint32_t)> fn);
  void apply_to_modes(uint32_t conn_id, std::function<void(uint32_t)> fn);

  int m_drm_fd;
  uint32_t m_banner_height;

  std::shared_ptr<drmModeRes> m_mode_resource;
  std::shared_ptr<drmModePlaneRes> m_plane_resource;

  qmap_t<uint32_t, std::shared_ptr<drm_crtc_t>> m_crtcs;
  qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> m_connectors;

  qlist_t<uint32_t> m_planes_available;
  qlist_t<uint32_t> m_crtcs_available;
  qlist_t<uint32_t> m_connectors_available;
  qlist_t<uint32_t> m_encoders_available;

  qlist_t<std::shared_ptr<drm_connector_t>> m_cloned_connectors;
  qlist_t<std::shared_ptr<drm_connector_t>> m_shared_connectors;
  qmap_t<uuid_t, qlist_t<std::shared_ptr<drm_connector_t>>> m_pinned_connectors;

  qlist_t<std::shared_ptr<display_plane_t>> m_cloned_display_planes;
  qlist_t<std::shared_ptr<display_plane_t>> m_shared_display_planes;
  qmap_t<uuid_t, qlist_t<std::shared_ptr<display_plane_t>>> m_pinned_display_planes;

  std::shared_ptr<desktop_plane_t> m_cloned_desktop{nullptr};
  std::shared_ptr<desktop_plane_t> m_shared_desktop{nullptr};
  qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> m_pinned_desktops;

  // IDs of the various components in the drmModeRes struct,
  // except the FBs, as on initialization, it is empty.
  gsl::span<uint32_t> m_plane_span;
  gsl::span<uint32_t> m_crtc_span;
  gsl::span<uint32_t> m_connector_span;
  gsl::span<uint32_t> m_encoder_span;
};

#endif // DRM_GPU__H
