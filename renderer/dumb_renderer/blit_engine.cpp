//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "blit_engine.h"

#include <emmintrin.h>
#include <mmintrin.h>
#include <pmmintrin.h>

#ifdef ENABLE_AVX
#include <immintrin.h>
#endif

#ifdef ENABLE_AVX
const int gAVXEnabled = 1;
const int BYTE_ALIGNMENT = 32;
#else
const int gAVXEnabled = 0;
const int BYTE_ALIGNMENT = 16;
#endif

#define ALIGNMENT_PROLOGUE_16BYTES(ptr, i, length) \
    for (; i < static_cast<int>(qMin(static_cast<quintptr>(length), ((4 - ((reinterpret_cast<quintptr>(ptr) >> 2) & 0x3)) & 0x3))); ++i)

static inline uint
BYTE_MUL(uint x, uint a)
{
    uint t = (x & 0xff00ff) * a;
    t = (t + ((t >> 8) & 0xff00ff) + 0x800080) >> 8;
    t &= 0xff00ff;

    x = ((x >> 8) & 0xff00ff) * a;
    x = (x + ((x >> 8) & 0xff00ff) + 0x800080);
    x &= 0xff00ff00;
    x |= t;
    return x;
}

/*
 * Multiply the components of pixelVector by alphaChannel
 * Each 32bits components of alphaChannel must be in the form 0x00AA00AA
 * colorMask must have 0x00ff00ff on each 32 bits component
 * half must have the value 128 (0x80) for each 32 bits compnent
 */
#define BYTE_MUL_SSE2(result, pixelVector, alphaChannel, colorMask, half)                       \
    {                                                                                           \
        /* 1. separate the colors in 2 vectors so each color is on 16 bits                      \
           (in order to be multiplied by the alpha                                              \
           each 32 bit of dstVectorAG are in the form 0x00AA00GG                                \
           each 32 bit of dstVectorRB are in the form 0x00RR00BB */                             \
        __m128i pixelVectorAG = _mm_srli_epi16(pixelVector, 8);                                 \
        __m128i pixelVectorRB = _mm_and_si128(pixelVector, colorMask);                          \
                                                                                                \
        /* 2. multiply the vectors by the alpha channel */                                      \
        pixelVectorAG = _mm_mullo_epi16(pixelVectorAG, alphaChannel);                           \
        pixelVectorRB = _mm_mullo_epi16(pixelVectorRB, alphaChannel);                           \
                                                                                                \
        /* 3. divide by 255, that's the tricky part.                                            \
           we do it like for BYTE_MUL(), with bit shift: X/255 ~= (X + X/256 + rounding)/256 */ \
        /** so first (X + X/256 + rounding) */                                                  \
        pixelVectorRB = _mm_add_epi16(pixelVectorRB, _mm_srli_epi16(pixelVectorRB, 8));         \
        pixelVectorRB = _mm_add_epi16(pixelVectorRB, half);                                     \
        pixelVectorAG = _mm_add_epi16(pixelVectorAG, _mm_srli_epi16(pixelVectorAG, 8));         \
        pixelVectorAG = _mm_add_epi16(pixelVectorAG, half);                                     \
                                                                                                \
        /** second divide by 256 */                                                             \
        pixelVectorRB = _mm_srli_epi16(pixelVectorRB, 8);                                       \
        /** for AG, we could >> 8 to divide followed by << 8 to put the                         \
            bytes in the correct position. By masking instead, we execute                       \
            only one instruction */                                                             \
        pixelVectorAG = _mm_andnot_si128(colorMask, pixelVectorAG);                             \
                                                                                                \
        /* 4. combine the 2 pairs of colors */                                                  \
        result = _mm_or_si128(pixelVectorAG, pixelVectorRB);                                    \
    }

#define BLEND_SOURCE_OVER_ARGB32_SSE2_helper(dst, srcVector, nullVector, half, one, colorMask, alphaMask) \
    {                                                                                                     \
        const __m128i srcVectorAlpha = _mm_and_si128(srcVector, alphaMask);                               \
        if (_mm_movemask_epi8(_mm_cmpeq_epi32(srcVectorAlpha, alphaMask)) == 0xffff) {                    \
            /* all opaque */                                                                              \
            _mm_store_si128((__m128i *) &dst[x], srcVector);                                              \
        } else if (_mm_movemask_epi8(_mm_cmpeq_epi32(srcVectorAlpha, nullVector)) != 0xffff) {            \
            /* not fully transparent */                                                                   \
            /* extract the alpha channel on 2 x 16 bits */                                                \
            /* so we have room for the multiplication */                                                  \
            /* each 32 bits will be in the form 0x00AA00AA */                                             \
            /* with A being the 1 - alpha */                                                              \
            __m128i alphaChannel = _mm_srli_epi32(srcVector, 24);                                         \
            alphaChannel = _mm_or_si128(alphaChannel, _mm_slli_epi32(alphaChannel, 16));                  \
            alphaChannel = _mm_sub_epi16(one, alphaChannel);                                              \
                                                                                                          \
            const __m128i dstVector = _mm_load_si128((__m128i *) &dst[x]);                                \
            __m128i destMultipliedByOneMinusAlpha;                                                        \
            BYTE_MUL_SSE2(destMultipliedByOneMinusAlpha, dstVector, alphaChannel, colorMask, half);       \
                                                                                                          \
            /* result = s + d * (1-alpha) */                                                              \
            const __m128i result = _mm_add_epi8(srcVector, destMultipliedByOneMinusAlpha);                \
            _mm_store_si128((__m128i *) &dst[x], result);                                                 \
        }                                                                                                 \
    }

#define BLEND_SOURCE_OVER_ARGB32_SSE2(dst, src, length, nullVector, half, one, colorMask, alphaMask)          \
    {                                                                                                         \
        int x = 0;                                                                                            \
                                                                                                              \
        /* First, get dst aligned. */                                                                         \
        ALIGNMENT_PROLOGUE_16BYTES(dst, x, length)                                                            \
        {                                                                                                     \
            uint s = src[x];                                                                                  \
            if (s >= 0xff000000)                                                                              \
                dst[x] = s;                                                                                   \
            else if (s != 0)                                                                                  \
                dst[x] = s + BYTE_MUL(dst[x], qAlpha(~s));                                                    \
        }                                                                                                     \
                                                                                                              \
        for (; x < length - 3; x += 4) {                                                                      \
            const __m128i srcVector = _mm_loadu_si128((__m128i *) &src[x]);                                   \
            BLEND_SOURCE_OVER_ARGB32_SSE2_helper(dst, srcVector, nullVector, half, one, colorMask, alphaMask) \
        }                                                                                                     \
        for (; x < length; ++x) {                                                                             \
            uint s = src[x];                                                                                  \
            if (s >= 0xff000000)                                                                              \
                dst[x] = s;                                                                                   \
            else if (s != 0)                                                                                  \
                dst[x] = s + BYTE_MUL(dst[x], qAlpha(~s));                                                    \
        }                                                                                                     \
    }

void
qt_blend_argb32_on_argb32_sse2(uchar *destPixels, int dbpl, const uchar *srcPixels, int sbpl, int w, int h)
{
    const quint32 *src = (const quint32 *) srcPixels;
    quint32 *dst = (quint32 *) destPixels;

    const __m128i alphaMask = _mm_set1_epi32(0xff000000);
    const __m128i nullVector = _mm_set1_epi32(0);
    const __m128i half = _mm_set1_epi16(0x80);
    const __m128i one = _mm_set1_epi16(0xff);
    const __m128i colorMask = _mm_set1_epi32(0x00ff00ff);

    for (int y = 0; y < h; ++y) {
        BLEND_SOURCE_OVER_ARGB32_SSE2(dst, src, w, nullVector, half, one, colorMask, alphaMask);
        dst = (quint32 *) (((uchar *) dst) + dbpl);
        src = (const quint32 *) (((const uchar *) src) + sbpl);
    }
}

void
slow_memcpy(void *dst, const void *src, int32_t len)
{
    asm("cld;"
        "rep;"
        "movsb"
        :
        : "S"(src), "D"(dst), "c"((size_t) len));
}

void
fast_memcpy(void *dst, const void *src, int32_t len)
{
    uint8_t *uint8_dst = (uint8_t *) dst;
    uint8_t *uint8_src = (uint8_t *) src;

    // ------------------------------------------------------------------------
    // Copy unaligned portion of the destination

    int32_t i = 0;
    int32_t alignment_dst = (uintptr_t)(&uint8_dst[i]) & (BYTE_ALIGNMENT - 1);

    if (alignment_dst != 0) {
        i = qMin(BYTE_ALIGNMENT - alignment_dst, len);
        slow_memcpy(dst, src, i);
    }

    if (i >= len)
        return;

    int32_t alignment_src = (uintptr_t)(&uint8_src[i]) & (BYTE_ALIGNMENT - 1);

// ------------------------------------------------------------------------
// Copy aligned portion of the destination

#ifndef ENABLE_AVX

    if (alignment_src != 0) {
        for (; i < len - 127; i += 128) {
            const __m128i xmm0 = _mm_loadu_si128((__m128i *) (&uint8_src[i]));
            const __m128i xmm1 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 16]));
            const __m128i xmm2 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 32]));
            const __m128i xmm3 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 48]));
            const __m128i xmm4 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 64]));
            const __m128i xmm5 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 80]));
            const __m128i xmm6 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 96]));
            const __m128i xmm7 = _mm_loadu_si128((__m128i *) (&uint8_src[i + 112]));

            _mm_stream_si128((__m128i *) (&uint8_dst[i]), xmm0);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 16]), xmm1);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 32]), xmm2);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 48]), xmm3);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 64]), xmm4);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 80]), xmm5);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 96]), xmm6);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 112]), xmm7);
        }

        for (; i < len - 15; i += 16) {
            const __m128i xmm0 = _mm_loadu_si128((__m128i *) (&uint8_src[i]));
            _mm_stream_si128((__m128i *) (&uint8_dst[i]), xmm0);
        }
    } else {
        for (; i < len - 127; i += 128) {
            const __m128i xmm0 = _mm_load_si128((__m128i *) (&uint8_src[i]));
            const __m128i xmm1 = _mm_load_si128((__m128i *) (&uint8_src[i + 16]));
            const __m128i xmm2 = _mm_load_si128((__m128i *) (&uint8_src[i + 32]));
            const __m128i xmm3 = _mm_load_si128((__m128i *) (&uint8_src[i + 48]));
            const __m128i xmm4 = _mm_load_si128((__m128i *) (&uint8_src[i + 64]));
            const __m128i xmm5 = _mm_load_si128((__m128i *) (&uint8_src[i + 80]));
            const __m128i xmm6 = _mm_load_si128((__m128i *) (&uint8_src[i + 96]));
            const __m128i xmm7 = _mm_load_si128((__m128i *) (&uint8_src[i + 112]));

            _mm_stream_si128((__m128i *) (&uint8_dst[i]), xmm0);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 16]), xmm1);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 32]), xmm2);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 48]), xmm3);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 64]), xmm4);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 80]), xmm5);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 96]), xmm6);
            _mm_stream_si128((__m128i *) (&uint8_dst[i + 112]), xmm7);
        }

        for (; i < len - 15; i += 16) {
            const __m128i xmm0 = _mm_load_si128((__m128i *) (&uint8_src[i]));
            _mm_stream_si128((__m128i *) (&uint8_dst[i]), xmm0);
        }
    }

#else

    if (alignment_src != 0) {
        for (; i < len - 255; i += 256) {
            const __m256i xmm0 = _mm256_loadu_si256((__m256i *) (&uint8_src[i]));
            const __m256i xmm1 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 32]));
            const __m256i xmm2 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 64]));
            const __m256i xmm3 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 96]));
            const __m256i xmm4 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 128]));
            const __m256i xmm5 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 160]));
            const __m256i xmm6 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 192]));
            const __m256i xmm7 = _mm256_loadu_si256((__m256i *) (&uint8_src[i + 224]));

            _mm256_stream_si256((__m256i *) (&uint8_dst[i]), xmm0);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 32]), xmm1);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 64]), xmm2);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 96]), xmm3);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 128]), xmm4);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 160]), xmm5);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 192]), xmm6);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 224]), xmm7);
        }

        for (; i < len - 31; i += 32) {
            const __m256i xmm0 = _mm256_loadu_si256((__m256i *) (&uint8_src[i]));
            _mm256_stream_si256((__m256i *) (&uint8_dst[i]), xmm0);
        }
    } else {
        for (; i < len - 255; i += 256) {
            const __m256i xmm0 = _mm256_load_si256((__m256i *) (&uint8_src[i]));
            const __m256i xmm1 = _mm256_load_si256((__m256i *) (&uint8_src[i + 32]));
            const __m256i xmm2 = _mm256_load_si256((__m256i *) (&uint8_src[i + 64]));
            const __m256i xmm3 = _mm256_load_si256((__m256i *) (&uint8_src[i + 96]));
            const __m256i xmm4 = _mm256_load_si256((__m256i *) (&uint8_src[i + 128]));
            const __m256i xmm5 = _mm256_load_si256((__m256i *) (&uint8_src[i + 160]));
            const __m256i xmm6 = _mm256_load_si256((__m256i *) (&uint8_src[i + 192]));
            const __m256i xmm7 = _mm256_load_si256((__m256i *) (&uint8_src[i + 224]));

            _mm256_stream_si256((__m256i *) (&uint8_dst[i]), xmm0);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 32]), xmm1);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 64]), xmm2);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 96]), xmm3);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 128]), xmm4);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 160]), xmm5);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 192]), xmm6);
            _mm256_stream_si256((__m256i *) (&uint8_dst[i + 224]), xmm7);
        }

        for (; i < len - 31; i += 32) {
            const __m256i xmm0 = _mm256_load_si256((__m256i *) (&uint8_src[i]));
            _mm256_stream_si256((__m256i *) (&uint8_dst[i]), xmm0);
        }
    }

#endif

    // ------------------------------------------------------------------------
    // Copy remaining portion

    if (len - i > 0) {
        slow_memcpy(&uint8_dst[i], &uint8_src[i], len - i);
    }
}

blit_engine g_blit_engine;

blit_engine::blit_engine()
{
    if (gAVXEnabled)
        qDebug() << "AVX Support is enabled";
}

void
blit_engine::blit_blend(QPainter &painter,
                        const std::shared_ptr<QImage> target,
                        const rect_t &trect,
                        const std::shared_ptr<QImage> source,
                        const rectf_t &srectf,
                        const region_t &clip)
{
    if (clip.isEmpty() == true || trect.isEmpty() == true || srectf.isEmpty() == true || !target || !source) {
        return;
    }

    auto clean_trect = target->rect() & trect;
    auto clean_srect = source->rect() & srectf.toRect();

    auto boundClip = clip & clean_trect;

    if (clean_trect.size() == clean_srect.size()) {
        auto bpp = source->depth() / 8;
        auto offsetX = clean_srect.x() - clean_trect.x();
        auto offsetY = clean_srect.y() - clean_trect.y();

        for (auto rect = boundClip.begin(); rect != boundClip.end(); rect++) {
            auto dstX = bpp * (rect->x());
            auto srcX = bpp * (rect->x() + offsetX);

            auto dstY = (rect->y());
            auto srcY = (rect->y() + offsetY);

            auto dstPixels = target->scanLine(dstY) + dstX;
            const auto srcPixels = source->scanLine(srcY) + srcX;

            qt_blend_argb32_on_argb32_sse2(dstPixels, target->bytesPerLine(), srcPixels, source->bytesPerLine(), rect->width(), rect->height());
        }
    } else {
        painter.setClipRegion(clip);
        painter.drawImage(trect, *source, srectf);
    }

    return;
}

void
blit_engine::blit_opaque(QPainter &painter,
                         const std::shared_ptr<QImage> target,
                         const rect_t &trect,
                         const std::shared_ptr<QImage> source,
                         const rectf_t &srectf,
                         const region_t &clip)
{
    if (clip.isEmpty() == true || trect.isEmpty() == true || srectf.isEmpty() == true || !target || !source) {
        return;
    }

    auto clean_trect = target->rect() & trect;
    auto clean_srect = source->rect() & srectf.toRect();

    auto boundClip = clip & clean_trect;

    if (clean_trect.size() == clean_srect.size()) {
        auto bpp = source->depth() / 8;
        auto offsetX = clean_srect.x() - clean_trect.x();
        auto offsetY = clean_srect.y() - clean_trect.y();

        for (auto rect = boundClip.begin(); rect != boundClip.end(); rect++) {
            auto dstX = bpp * (rect->x());
            auto srcX = bpp * (rect->x() + offsetX);

            auto srcWidth = rect->width() * bpp;

            for (auto h = rect->top(); h <= rect->bottom(); h++) {
                auto dstY = (h);
                auto srcY = (h + offsetY);

                auto dstPixels = target->scanLine(dstY) + dstX;
                const auto srcPixels = source->scanLine(srcY) + srcX;

                fast_memcpy(dstPixels, srcPixels, srcWidth);
            }
        }
    } else {
        painter.setCompositionMode(QPainter::CompositionMode_Source);
        painter.setClipRegion(clip);
        painter.drawImage(trect, *source, srectf);
    }

    return;
}

void
blit_engine::blit_opaque(QPainter &painter,
                         const std::shared_ptr<QImage> target,
                         const rect_t &trect,
                         const QImage &source,
                         const rectf_t &srectf,
                         const region_t &clip)
{
    if (clip.isEmpty() == true || trect.isEmpty() == true || srectf.isEmpty() == true || !target) {
        return;
    }

    auto clean_trect = target->rect() & trect;
    auto clean_srect = source.rect() & srectf.toRect();

    auto boundClip = clip & clean_trect;

    if (clean_trect.size() == clean_srect.size()) {
        auto bpp = source.depth() / 8;
        auto offsetX = clean_srect.x() - clean_trect.x();
        auto offsetY = clean_srect.y() - clean_trect.y();

        for (auto rect = boundClip.begin(); rect != boundClip.end(); rect++) {
            auto dstX = bpp * (rect->x());
            auto srcX = bpp * (rect->x() + offsetX);

            auto srcWidth = rect->width() * bpp;

            for (auto h = rect->top(); h <= rect->bottom(); h++) {
                auto dstY = (h);
                auto srcY = (h + offsetY);

                auto dstPixels = target->scanLine(dstY) + dstX;
                const auto srcPixels = source.scanLine(srcY) + srcX;

                fast_memcpy(dstPixels, srcPixels, srcWidth);
            }
        }
    } else {
        painter.setCompositionMode(QPainter::CompositionMode_Source);
        painter.setClipRegion(clip);
        painter.drawImage(trect, source, srectf);
    }

    return;
}
