//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef LONG_PRESS_FILTER_H
#define LONG_PRESS_FILTER_H

#include <inputserverfilter.h>
#include <QDateTime>

class long_press_filter_t : public input_server_filter_t
{
    Q_OBJECT

public:

    long_press_filter_t(uint32_t key_code, std::shared_ptr<input_action_t> action, uint16_t ms_threshold = 1000);
    virtual ~long_press_filter_t(void);

    virtual bool filter_event(std::shared_ptr<vm_t> &vm, xt_input_event *event);

signals:

    void out_of_band_event(std::shared_ptr<vm_t> target_vm, xt_input_event event);

private:

    bool completes_long_press(xt_input_event *event);

    uint32_t m_key_code;
    std::shared_ptr<input_action_t> m_action;
    uint16_t m_threshold;
    int64_t m_last_event_time;
    xt_input_event m_last_event;
};

#endif // LONG_PRESS_FILTER_H
