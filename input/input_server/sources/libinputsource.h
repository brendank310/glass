//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef LIBINPUTSOURCE__H
#define LIBINPUTSOURCE__H

extern "C" {
#include <fcntl.h>
#include <unistd.h>
#include <libudev.h>
#include "linux/input.h"
#include <sys/ioctl.h>
#include <sys/signalfd.h>
#include <poll.h>
#include <libinput.h>
}

#include <inputsource.h>
#include <QDebug>
#include <QSocketNotifier>
#include <xt_input_global.h>
#include <window_manager.h>

#define LI_CTX ((struct libinput *)m_libinput_ctx)
#define UDEV_CTX ((struct udev *)m_libudev_ctx)

class libinput_source_t : public input_source_t
{
    Q_OBJECT

public:

    libinput_source_t(window_manager_t &wm);
    ~libinput_source_t(void);

signals:

    void device_added(QString device);
    void device_removed(QString device);
    void add_pointer_device(void);
    void add_multitouch_device(void);

public slots:

    virtual void set_mouse_speed(float mouse_speed);
    virtual void set_touchpad_tap_to_click(bool enabled);
    virtual void set_touchpad_scrolling(bool enabled);
    virtual void set_touchpad_speed(float touchpad_speed);
    virtual void set_led_code(const uint32_t led_code);
    virtual void receive_event(void *event);

    virtual void set_tap_input(bool enabled);
    virtual void clear_tap_input();

private slots:

    void libinput_socket_event(int fd);

private:

    void populate_absolute_event(struct xt_input_event *event, struct libinput_event *input_event);
    void handle_key_event(struct libinput_event *ev);
    void handle_relative_mouse_event(struct libinput_event *ev);
    void handle_absolute_mouse_event(struct libinput_event *ev);
    void handle_mouse_button_event(struct libinput_event *ev);
    void handle_scroll_event(struct libinput_event *ev);

    //multitouch
    void handle_multitouch_event(struct libinput_event *ev);
    void send_multitouch_event(struct libinput_event *ev, uint32_t event_type);

    //tablet tool
    void handle_tablet_tool_event(struct libinput_event *ev);
    void send_tablet_tool_event(struct libinput_event *ev);

    void send_absolute_movement(struct libinput_event *ev);


    #ifdef TAP_INPUT
        void write_event(uint16_t type, uint16_t code, int32_t value);
        void write_sync_event();
    #endif

    std::unique_ptr<QSocketNotifier> m_input_socket;
    int32_t m_input_fd;
    libinput *m_libinput_ctx;
    udev *m_libudev_ctx;
    QList<struct libinput_device *> m_devices;    
    window_manager_t &m_wm;

    #ifdef TAP_INPUT
        int32_t m_tap_fd;
        bool tap_input_enabled;
    #endif
};

#endif //LIBINPUTSOURCE__H
