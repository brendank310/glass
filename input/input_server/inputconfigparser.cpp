//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <inputconfigparser.h>
#include <switchvmaction.h>
#include <hideswitchaction.h>
#include <advanceswitcheraction.h>
#include <showswitchaction.h>
#include <screenshotaction.h>
#include <brightnessaction.h>
#include <exitaction.h>
#include <rtcmaction.h>

input_config_parser_t::input_config_parser_t(json &obj, input_server_t *input_server) : m_config(obj), m_input_server(input_server)
{
}

input_config_parser_t::~input_config_parser_t(void)
{

}

void
input_config_parser_t::create_filters_from_config()
{
    std::shared_ptr<input_server_filter_t> filter;
    auto action_array = m_config["shortcut_actions"];

    for (auto &action_object : action_array) {
        QString name = QString::fromStdString(action_object["name"]);

        if (name.startsWith("slot")) {
            filter = create_slot_filter(action_object);
        } else if (name.startsWith("vm_switcher")) {
            filter = create_switcher_filter(action_object);
        } else if (name == "save_screenshot") {
            filter = create_screenshot_filter(action_object);
        } else if (name.startsWith("brightness")) {
            filter = create_brightness_filter(action_object);
        } else if (name.startsWith("longpress")) {
            filter = create_long_press_filter(action_object);
        } else if (name == "exit") {
            filter = create_exit_filter(action_object);
        } else if (name == "idle-timer") {
            filter = create_idle_filter(action_object);
        } else if (name == "revert_to_cloned_mode") {
            filter = create_revert_to_cloned_mode_filter(action_object);
        } else {
            filter = NULL;
        }

        if (filter != NULL) {
            m_input_server->register_input_filter(name, filter);
        }
     }

    return;
}

QList<QString>
input_config_parser_t::parse_whitelist(void)
{
    QList<QString> temp;

    return temp;
}

QPair<KeyMask, KeyMask>
input_config_parser_t::get_base_secondary_pair(json entry)
{
    xt_input_event ev;
    std::shared_ptr<KeyMask> base_mask = std::make_shared<KeyMask>();
    std::shared_ptr<KeyMask> secondary_mask = std::make_shared<KeyMask>();
    QPair<KeyMask, KeyMask> mask_pair;

    CLEAR_ALL_KEYS(*base_mask);
    CLEAR_ALL_KEYS(*secondary_mask);

    for (auto key  : entry["basemask"]) {
        QString key_string = QString::fromStdString(key);

        if (key_string.isEmpty()) continue;

        memset(&ev, 0x00, sizeof(ev));
        ev.keyCode = string_to_keycode[key_string];
        ev.flags = 1;
        SET_KEY(*base_mask, ev);
    }

    for (auto key : entry["secondarymask"]) {
        QString key_string = QString::fromStdString(key);

        if (key_string.isEmpty()) continue;

        memset(&ev, 0x00, sizeof(ev));
        ev.keyCode = string_to_keycode[key_string];
        ev.flags = 1;
        SET_KEY(*secondary_mask, ev);
    }

    if (secondary_mask != NULL && base_mask != NULL) {
        mask_pair = QPair<KeyMask, KeyMask>(*base_mask, *secondary_mask);
    }

    return mask_pair;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_switcher_filter(json action)
{
    auto triggers = action["triggers"];
    QString direction = QString::fromStdString(action["action"]);
    std::shared_ptr<advance_switcher_action_t> advanced_switcher_action = std::make_shared<advance_switcher_action_t>();
    std::shared_ptr<show_switch_action_t> show_switcher_action =  std::make_shared<show_switch_action_t>();
    std::shared_ptr<hide_switch_action_t> hide_switcher_action =  std::make_shared<hide_switch_action_t>();

    if (direction == "forward") {
        connect(advanced_switcher_action.get(), &advance_switcher_action_t::advance_switcher,
                show_switcher_action.get(), &show_switch_action_t::advance_highlight_vm_right);

    } else if (direction == "backward") {
        connect(advanced_switcher_action.get(), &advance_switcher_action_t::advance_switcher,
                show_switcher_action.get(), &show_switch_action_t::advance_highlight_vm_left);

    } else {
        return NULL;
    }

    connect(show_switcher_action.get(), &show_switch_action_t::highlighted_vm_changed, hide_switcher_action.get(), &hide_switch_action_t::vm_changed);
    connect(hide_switcher_action.get(), &hide_switch_action_t::hide_switcher, show_switcher_action.get(), &show_switch_action_t::reset_switcher);
    connect(show_switcher_action.get(), &show_switch_action_t::show_vm_switcher, m_input_server, &input_server_t::show_switcher);
    connect(hide_switcher_action.get(), &hide_switch_action_t::hide_switcher, m_input_server, &input_server_t::hide_switcher);
    connect(show_switcher_action.get(), &show_switch_action_t::advance_vm_right, m_input_server, &input_server_t::advance_vm_right);
    connect(show_switcher_action.get(), &show_switch_action_t::advance_vm_left, m_input_server, &input_server_t::advance_vm_left);
    connect(m_input_server, &input_server_t::highlight_vm_changed, show_switcher_action.get(), &show_switch_action_t::set_highlight_vm);

    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            show_switcher_action,
                                                                                            advanced_switcher_action,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            hide_switcher_action);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_slot_filter(json j)
{
    auto triggers = j["triggers"];
    std::string slot_string = j["action"];
    int32_t slot = std::stoi(slot_string);

    std::shared_ptr<switch_vm_action_t> switch_vm_action = std::make_shared<switch_vm_action_t>(slot);
    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            switch_vm_action,
                                                                                            nullptr,
                                                                                            switch_vm_action);

    connect(switch_vm_action.get(), &switch_vm_action_t::switch_vm, m_input_server, &input_server_t::switch_vm);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_idle_filter(json action)
{
    auto triggers = action["triggers"];
    std::shared_ptr<idle_filter_t> idle_filter = std::make_shared<idle_filter_t>();

    connect(idle_filter.get(), &idle_filter_t::idle_timeout, m_input_server, &input_server_t::idle_timeout);
    connect(m_input_server, &input_server_t::idle_timer_update, idle_filter.get(), &idle_filter_t::idle_timer_update);


    idle_filter->clear_triggers();

    for (auto &trigger : triggers) {
        idle_filter->add_trigger(trigger);
    }

    return idle_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_long_press_filter(json action)
{
    (void)action;
    // If this ever gets implemented, don't forget to pay attention to the "disabled" field in the trigger.
    // It's not being done in the filter class like the rest, so it needs to be checked here.
    return NULL;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_brightness_filter(json j)
{
    auto triggers = j["triggers"];
    std::string action = j["action"];

    std::shared_ptr<input_action_t> input_action = std::make_shared<brightness_action_t>(action);
    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            input_action);

    connect(dynamic_cast<brightness_action_t *>(input_action.get()), &brightness_action_t::increase_brightness, m_input_server, &input_server_t::increase_brightness);
    connect(dynamic_cast<brightness_action_t *>(input_action.get()), &brightness_action_t::decrease_brightness, m_input_server, &input_server_t::decrease_brightness);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_screenshot_filter(json j)
{
    auto triggers = j["triggers"];

    std::shared_ptr<input_action_t> input_action = std::make_shared<screenshot_action_t>();
    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            input_action);

    connect(dynamic_cast<screenshot_action_t *>(input_action.get()), &screenshot_action_t::save_screenshot, m_input_server, &input_server_t::save_screenshot);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_exit_filter(json j)
{
    auto triggers = j["triggers"];

    std::shared_ptr<input_action_t> input_action = std::make_shared<exit_action_t>();
    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            input_action);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

std::shared_ptr<input_server_filter_t>
input_config_parser_t::create_revert_to_cloned_mode_filter(json j)
{
    auto triggers = j["triggers"];

    std::shared_ptr<input_action_t> input_action = std::make_shared<revert_to_cloned_mode_action_t>();
    std::shared_ptr<input_server_filter_t> key_filter = std::make_shared<key_stroke_filter_t>(
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            nullptr,
                                                                                            input_action);

    key_filter->clear_triggers();

    for (auto trigger : triggers) {
        key_filter->add_trigger(trigger);
    }

    return key_filter;
}

