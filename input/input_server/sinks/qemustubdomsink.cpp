//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <qemustubdomsink.h>

qemu_stubdom_sink_t::qemu_stubdom_sink_t(domid_t domid)
{
    (void)domid;
}

qemu_stubdom_sink_t::~qemu_stubdom_sink_t()
{

}

void
qemu_stubdom_sink_t::set_inactive()
{

}

void
qemu_stubdom_sink_t::set_active()
{
}

void
qemu_stubdom_sink_t::enqueue_input_event(xt_input_event event)
{
    (void)event;
}

void
qemu_stubdom_sink_t::handle_pause_break_key(xt_input_event event)
{
    (void)event;
}

void
qemu_stubdom_sink_t::inject_extended_prefix(xt_input_event event)
{
    (void)event;
}

point_t
qemu_stubdom_sink_t::scale_absolute_event(const xt_input_event &event)
{
    (void)event;

    //TODO: This is only here so that the compiler wont complain. Remove me!
    point_t p;
    return p;
}
