//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <vkbdsingleton.h>
#include <xenbackend.h>

vkbd_singleton_t::vkbd_singleton_t(int32_t backend_id) : m_backend_domid(backend_id), m_device_count(0), m_xenstore_event(NULL)
{
    Q_ASSERT(backend_init(m_backend_domid) == 0);

    // Create a pointer to a socket notifier so we can monitor a socket. Error out if we are unable to
    m_xenstore_event = std::make_shared<QSocketNotifier>(backend_xenstore_fd(), QSocketNotifier::Read);

    Q_CHECK_PTR(m_xenstore_event);

    // Whenever a socket event occurs(a read on our fd) call our backend_event slot
    connect(m_xenstore_event.get(), &QSocketNotifier::activated, this, &vkbd_singleton_t::backend_event, Qt::QueuedConnection);
}

vkbd_singleton_t::~vkbd_singleton_t(void)
{
    for (auto back_iter : m_backend_list) {
        m_backend_list.removeOne(back_iter);
        m_device_count--;
    }
}

void
vkbd_singleton_t::register_backend(xen_vkbd_backend_t* backend)
{
    m_backend_list.append(backend);
    m_device_count++;
}

void
vkbd_singleton_t::unregister_backend(xen_vkbd_backend_t* backend)
{
    // Iterate over the backend list until we find the device in question, then remove it
    for (auto back_iter : m_backend_list) {
        if (backend == back_iter) {
            // Only remove one instance of this object at a time
            m_backend_list.removeOne(backend);
            m_device_count--;
            return;
        }
    }
}

void
vkbd_singleton_t::backend_event(int32_t fd)
{
    void *unused = NULL;

    Q_ASSERT(fd >= 0);

    backend_xenstore_handler(unused);
}
