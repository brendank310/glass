//
// DB Watch
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================
#include <memory>
#include <QtCore>
#include <dbwatch.h>
#include <db_interface.h>

// ============================================================================
// db_watch Implementation
// ============================================================================

db_watch::db_watch() : m_timeout(60000), m_status(STOPPED) {
    m_db = std::make_unique<db_dbus_t>(QString("com.citrix.xenclient.db"),
                                       QString("/"),
                                       QDBusConnection::systemBus());
    m_timer = std::make_unique<QTimer>(this);

    connect(m_timer.get(), &QTimer::timeout, this, &db_watch::update);
}

db_watch::~db_watch() {
    m_timer->stop();
}

const QStringList &db_watch::paths() const {
    return m_paths;
}

const QString db_watch::path() const {
    qDebug() << "db_watch::path() is deprecated, please use db_watch::paths() instead.";
    if (m_paths.isEmpty()) {
        return QString();
    }
    return m_paths[0];
}

void db_watch::addPath(const QString &path) {
    if (path.isEmpty()) {
        return;
    }
    if (!path.startsWith("/")) {
        return;
    }
    if (m_paths.contains(path)) {
        return;
    }
    if (m_status != db_watch::STOPPED) {
        qWarning() << "paths can't be added while db_watch is running";
        return;
    }

    m_paths.append(path);
}

void db_watch::setPath(const QString &path) {
    qDebug() << "db_watch::setPath() is deprecated, please use db_watch::addPath() instead.";
    addPath(path);
}

void db_watch::removePath(const QString &path) {
    if (path.isEmpty()) {
        return;
    }
    if (m_status != db_watch::STOPPED) {
        qWarning() << "paths can't be removed while db_watch is running";
        return;
    }

    int index = m_paths.indexOf(path);
    m_paths.removeAt(index);
}

db_watch::STATUS db_watch::status() const {
    return m_status;
}

bool db_watch::start() {
    if (m_paths.isEmpty()) {
        return false;
    }
    if (m_status != db_watch::STOPPED) {
        return false;
    }

    for (int i = 0; i < m_paths.count(); ++i) {
        m_exists.insert(i, m_db->exists(m_paths[i]));

        if (m_exists[i]) {
            m_values.insert(i, m_db->read(m_paths[i]));
        } else {
            m_values.insert(i, QString());
        }
    }

    m_timer->start(m_timeout);
    m_status = db_watch::RUNNING;

    return true;
}

void db_watch::stop() {
    if (m_status != db_watch::RUNNING) {
        return;
    }

    m_timer->stop();
    m_status = db_watch::STOPPED;

    m_exists.clear();
    m_values.clear();
}

void db_watch::update() {
    if (m_paths.isEmpty()) {
        return;
    }
    if (m_status != db_watch::RUNNING) {
        return;
    }

    for (int i = 0; i < m_paths.count(); ++i) {
        bool old_exists = m_exists.takeAt(i);
        QString old_value = m_values.takeAt(i);

        m_exists.insert(i, m_db->exists(m_paths[i]));

        if (m_exists[i]) {
            m_values.insert(i, m_db->read(m_paths[i]));
        } else {
            m_values.insert(i, QString());
        }

        if (m_exists[i] != old_exists || m_values[i] != old_value) {
            emit triggered(m_paths[i]);
        }
    }
}
