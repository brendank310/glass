//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <cstdlib>
#include <exitaction.h>


exit_action_t::exit_action_t(void)
{

}

exit_action_t::~exit_action_t(void)
{

}

void
exit_action_t::operator()()
{
    std::exit(0);
}
