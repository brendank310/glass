//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef SCREENSHOT_ACTION__H
#define SCREENSHOT_ACTION__H

#include <inputaction.h>

class screenshot_action_t : public input_action_t
{
    Q_OBJECT

public:

    screenshot_action_t();
    ~screenshot_action_t();

    void operator()();

signals:

    void save_screenshot();

private:

    QString m_slot;
};

#endif //SCREENSHOT_ACTION__H
