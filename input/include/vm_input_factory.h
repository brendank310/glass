//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_INPUT_FACTORY__H
#define VM_INPUT_FACTORY__H

#include <memory>

#include <vm_input.h>
#include <window_manager.h>
#include <QObject>

class vm_input_factory_t : public QObject
{
    Q_OBJECT;

public:
	vm_input_factory_t(window_manager_t &wm) : m_wm(wm) {}
	virtual ~vm_input_factory_t() = default;
	virtual std::shared_ptr<vm_input_t> make_vm_input(std::shared_ptr<vm_base_t> vm) = 0;

protected:
  window_manager_t &m_wm;
};

#endif // VM_INPUT_FACTORY__H
