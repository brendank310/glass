#!/bin/bash -x

git clone https://ci-token:${TOKEN}@gitlab.com/vglass/ivc.git
cd ivc
PREFIX=/usr make install_user
cd ..

git clone https://ci-token:${TOKEN}@gitlab.com/vglass/pv-display-helper.git
cd pv-display-helper
PREFIX=/usr make backend install_backend
cd ..

git clone https://github.com/OpenXT/libxenbackend.git
cd libxenbackend
autoreconf -i .
./configure --prefix=/usr
make
make install
cd ..
