//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <fs_vm_region_factory.h>

fs_vm_region_factory_t::fs_vm_region_factory_t(window_manager_t &wm) : vm_region_factory_t(wm)
{
    QObject::connect(this, &fs_vm_region_factory_t::vm_region_added, &wm, &window_manager_t::add_guest);
}

std::shared_ptr<vm_region_t>
fs_vm_region_factory_t::make_vm_region(std::shared_ptr<vm_base_t> &vm)
{
    std::shared_ptr<vm_region_t> vm_region = std::make_shared<fs_vm_region_t>(vm);
    emit vm_region_added(vm_region);
    return vm_region;
}
