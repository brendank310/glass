//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <desktop_plane.h>

desktop_plane_t::desktop_plane_t(uuid_t uuid, rect_t rect, point_t plane_origin, bool renderable) : plane_t(rect, plane_origin), m_renderable(renderable), m_uuid(uuid) {}

desktop_plane_t::~desktop_plane_t()
{
    m_current_display = nullptr;
    m_displays.clear();
    m_render_targets.clear();
    m_qemu_source.clear();
}

void
desktop_plane_t::add_display(std::shared_ptr<display_plane_t> display)
{
    if (!display) {
        return;
    }

    for(auto &d : m_displays) {
        if (!d) {
            continue;
        }

        if(d.get() == display.get()) {
            return;
        }
    }

    rect_t extent = display->parent_rect();

    m_visible_region += extent;

    vg_info() << display.get() << extent;

    // Take care of the math-y portion first
    m_plane += extent;

    set_origin(m_plane.boundingRect().topLeft());

    // Now some resource organization stuff
    m_displays.push_back(display);
}

void
desktop_plane_t::add_render_target(uuid_t uuid, std::unique_ptr<render_target_plane_t> render_target)
{
    render_targets(uuid).push_back(std::move(render_target));

    m_visible_region += render_targets(uuid).back()->parent_rect();
}

display_plane_t *
desktop_plane_t::display(point_t point)
{
  for(auto &display : m_displays) {
      if (!display) {
          continue;
      }

      if(display->parent_rect().contains(point)) {
          return display.get();
      }
  }

  return nullptr;
}

display_plane_t *
desktop_plane_t::display(uint32_t key)
{
    for(auto &d : m_displays) {
        if (!d) {
            continue;
        }

        if(d->unique_id() == key) {
            return d.get();
        }
    }

    return nullptr;
}

void
desktop_plane_t::dump_render_targets(uuid_t uuid)
{
    vg_debug() << this << uuid << ": #rtps -> " << render_targets(uuid).size();
    for(auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            vg_debug() << "nullptr stored in render targets for " << uuid;
            continue;
        }

        vg_debug() << uuid << ": " << rtp->origin() << rtp->rect();
    }
}


render_target_plane_t *
desktop_plane_t::render_target(uuid_t uuid, point_t point)
{
    for(auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if(rtp->parent_rect().contains(point)) {
            return rtp.get();
        }
    }

    return nullptr;
}

render_target_plane_t *
desktop_plane_t::render_target(uuid_t uuid, display_plane_t *display)
{
    if(!display) {
        return nullptr;
    }

    if(render_targets(uuid).size() == 1) {
        for(auto &rtp : render_targets(uuid)) {
            if (!rtp) {
                continue;
            }

            if(rtp->parent_rect().intersects(display->parent_rect())) {
                    return rtp.get();
            }
        }
    }

    for(auto &rtp : render_targets(uuid)) {
        if(!rtp) {
            continue;
        }

        if(rtp->rect().size() == display->render_plane().rect().size() && rtp->key() == display->unique_id()) {
            return rtp.get();
        }
    }

    return nullptr;
}

render_target_plane_t * desktop_plane_t::render_target(uuid_t uuid, window_key_t key)
{
    for (auto &rtp : render_targets(uuid)) {
        if (!rtp) {
            continue;
        }

        if (rtp->key() == key) {
            return rtp.get();
        }
    }
    return nullptr;
}

render_source_plane_t * desktop_plane_t::render_source(uuid_t uuid, window_key_t key)
{
    auto rtp = render_target(uuid, key);
    if (!rtp) return nullptr;
    return rtp->render_source();
}

list_t<std::shared_ptr<display_plane_t>> &
desktop_plane_t::displays()
{
    return m_displays;
}

hash_t<uuid_t, list_t<std::unique_ptr<render_target_plane_t>>>&
desktop_plane_t::render_targets()
{
    return m_render_targets;
}

list_t<std::unique_ptr<render_target_plane_t>>&
desktop_plane_t::render_targets(uuid_t uuid)
{
    return m_render_targets[uuid];
}

void
desktop_plane_t::remove_render_target(uuid_t uuid, render_target_plane_t *render_target)
{
    m_render_targets[uuid].remove_if([&](const std::unique_ptr<render_target_plane_t> &rtp) { return render_target == rtp.get(); });
}

bool
desktop_plane_t::renderable()
{
    return m_renderable;
}

void
desktop_plane_t::remove_render_targets(uuid_t uuid)
{
    m_render_targets[uuid].clear();
}

region_t &
desktop_plane_t::visible_region()
{
    return m_visible_region;
} 

void desktop_plane_t::remove_guest(uuid_t uuid)
{
    m_qemu_source[uuid] = nullptr;
}

transform_t
desktop_plane_t::from(plane_t &source_plane)
{
    return translate(source_plane);
}

transform_t
desktop_plane_t::to(plane_t &source_plane)
{
    return from(source_plane).inverted();
}

transform_t
desktop_plane_t::translate(plane_t &source_plane)
{
    transform_t translate_transform;
    qreal sx = source_plane.origin().x();
    qreal sy = source_plane.origin().y();

    translate_transform.translate(sx, sy);

    return translate_transform;
}

point_t
desktop_plane_t::map_to(plane_t *source_plane, point_t point)
{
    if(!source_plane) {
        return point_t(0,0);
    }

    return to(*source_plane).map(point);
}

point_t
desktop_plane_t::map_from(plane_t *source_plane, point_t point)
{
    Expects(source_plane);

    return from(*source_plane).map(point);
}

rect_t
desktop_plane_t::map_to(plane_t *source_plane, rect_t rect)
{
    if(!source_plane) {
        return rect_t(0, 0, 0, 0);
    }

    return to(*source_plane).mapRect(rect);
}

rect_t
desktop_plane_t::map_from(plane_t *source_plane, rect_t rect)
{
    Expects(source_plane);

    return from(*source_plane).mapRect(rect);
}

region_t
desktop_plane_t::map_to(plane_t *source_plane, region_t region)
{
    if(!source_plane) {
        return region_t();
    }

    return to(*source_plane).map(region);
}

region_t
desktop_plane_t::map_from(plane_t *source_plane, region_t region)
{
    Expects(source_plane);

    return from(*source_plane).map(region);
}

void
desktop_plane_t::reorigin_displays()
{
    vg_info() << "Reorigin displays for plane" << m_uuid;
    for(auto display : m_displays) {
        if (!display) {
            continue;
        }
        vg_info() << "before: " << origin() << display->origin();
        display->set_origin(display->origin() - origin());
        vg_info() << "after: " << origin() << display->origin();
    }
}

void
desktop_plane_t::translate_planes()
{
    m_plane = m_plane.translated(-origin().x(), -origin().y());
    m_visible_region = m_visible_region.translated(-origin().x(), -origin().y());
}

void desktop_plane_t::set_qemu_source(uuid_t uuid, std::shared_ptr<render_source_plane_t> qemu)
{
    m_qemu_source[uuid] = qemu;
}

void desktop_plane_t::attach_qemu_source(uuid_t uuid)
{
    qDebug() << "reattaching qemu surfaces";
    if (m_qemu_source[uuid]) {
        for (auto &rtp : render_targets(uuid)) {
            if (!rtp) {
                continue;
            }

            rtp->attach_render_source(m_qemu_source[uuid], true);
            rtp->force_qemu_render_source(true);
        }
    }
    else
        vg_info() <<__FUNCTION__ << ": no qemu surface to reattach!";
}
