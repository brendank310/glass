//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <input_plane.h>

input_plane_t::input_plane_t(rect_t rect, point_t plane_origin) : plane_t(rect, plane_origin),
                                                                  m_hotspot(-1, -1)
{
}

rect_t
input_plane_t::rect()
{
    return rect_t(point_t(0, 0), m_plane.boundingRect().size());
}

void
input_plane_t::add_desktop(std::shared_ptr<desktop_plane_t> plane)
{
    if (!plane) {
        return;
    }

    if(plane->renderable() && hotspot() == point_t(-1, -1)) {
        for(auto &display : plane->displays()) {
            if (!display) {
                continue;
            }

            rect_t extents(display->origin(), display->rect().size());
            hotspot() = display->origin() + plane->origin();
            break;
        }
    }


    // Manage geometry
    rect_t extents = rect_t(plane->origin().x(),
                            plane->origin().y(),
                            plane->rect().width(),
                            plane->rect().height());

    m_plane += extents;
    m_desktop_extents[extents] = plane;

    // Manage resources
    m_desktop_planes[plane->uuid()] = plane;
}

void
input_plane_t::reset()
{
    m_plane &= rect_t();
    m_desktop_extents.clear();
    m_desktop_planes.clear();
    m_current_desktop = nullptr;
}

qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> &
input_plane_t::desktops()
{
    return m_desktop_planes;
}

std::shared_ptr<desktop_plane_t>
input_plane_t::desktop(point_t point)
{
    for (auto extent : m_desktop_extents.keys()) {
        if (extent.contains(point)) {
            return m_desktop_extents[extent];
        }
    }

    return nullptr;
}

transform_t
input_plane_t::from(desktop_plane_t &source_plane)
{
    return translate(source_plane);
}

transform_t
input_plane_t::to(desktop_plane_t &source_plane)
{
    return from(source_plane).inverted();
}

transform_t
input_plane_t::translate(desktop_plane_t &source_plane)
{
    transform_t translate_transform;
    qreal sx = source_plane.origin().x();
    qreal sy = source_plane.origin().y();

    translate_transform.translate(sx, sy);

    return translate_transform;
}

point_t
input_plane_t::map_to(desktop_plane_t *source_plane, point_t point)
{
    if(!source_plane) {
        return point_t(0,0);
    }

    return to(*source_plane).map(point);
}

point_t
input_plane_t::map_from(desktop_plane_t *source_plane, point_t point)
{
    Expects(source_plane);

    return from(*source_plane).map(point);
}

rect_t
input_plane_t::map_to(desktop_plane_t *source_plane, rect_t rect)
{
    if(!source_plane) {
        return rect_t(0, 0, 0, 0);
    }

    return to(*source_plane).mapRect(rect);
}

rect_t
input_plane_t::map_from(desktop_plane_t *source_plane, rect_t rect)
{
    Expects(source_plane);

    return from(*source_plane).mapRect(rect);
}

region_t
input_plane_t::map_to(desktop_plane_t *source_plane, region_t region)
{
    if(!source_plane) {
        return region_t();
    }

    return to(*source_plane).map(region);
}

region_t
input_plane_t::map_from(desktop_plane_t *source_plane, region_t region)
{
    Expects(source_plane);

    return from(*source_plane).map(region);
}
